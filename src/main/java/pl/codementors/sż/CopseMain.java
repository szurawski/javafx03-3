package pl.codementors.sż;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CopseMain extends Application{

    
    public static void main(String[] args) {
        launch(args);//Start JavaFX context.
    }
    @Override
    public void start(Stage stage) throws Exception {
        //Here fxml file should be read and scene created.

        URL fxml = this.getClass().getResource("/pl/codementors/sż/ents_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("pl.codementors.sż.ents_list_msg");

        //To crete root object fxml file and resource bundle are required.
        Parent root = FXMLLoader.load(fxml, rb);

        //Create scene using root generated from fxml.
        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("/pl/codementors/sż/ents_list.css");
        stage.setTitle(rb.getString("applicationTitle"));
        stage.setScene(scene);
        stage.show();
    }

//    @Override
//    public void start(Stage stage) throws Exception {
//        //Here fxml file should be read and scene created.
//        CopseContext.create(stage);
//        stage.show();
//
//    }
}



