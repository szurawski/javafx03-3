package pl.codementors.sż;

import java.io.Serializable;

public class Ent implements Serializable {

    private int rokPosadzenia;

    private int wysokosc;

    private String gatunek;



    public enum Typ {
        LISCIASTE,
        IGLASTE;
    }

    private Typ typ;

    public Ent(int rokPosadzenia, int wysokosc, String gatunek, Typ typ) {
        this.rokPosadzenia = rokPosadzenia;
        this.wysokosc = wysokosc;
        this.gatunek = gatunek;
        this.typ = typ;
    }

    public Ent() {
    }

    public int getRokPosadzenia() {
        return rokPosadzenia;
    }

    public void setRokPosadzenia(int rokPosadzenia) {
        this.rokPosadzenia = rokPosadzenia;
    }

    public int getWysokosc() {
        return wysokosc;
    }

    public void setWysokosc(int wysokosc) {
        this.wysokosc = wysokosc;
    }

    public String getGatunek() {
        return gatunek;
    }

    public void setGatunek(String gatunek) {
        this.gatunek = gatunek;
    }

    public Typ getTyp() {
        return typ;
    }

    public void setTyp(Typ typ) {
        this.typ = typ;
    }


}

