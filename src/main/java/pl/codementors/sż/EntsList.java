package pl.codementors.sż;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.sż.workers.OpenFile;
import pl.codementors.sż.workers.SaveFile;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

//import static pl.codementors.sż.CopseContext.ents;

public class EntsList implements Initializable {


    private static final Logger log = Logger.getLogger(EntsList.class.getCanonicalName());

    @FXML
    private TableView entsTable;
    private Ent ent;

    @FXML
    private TableColumn<Ent, Ent.Typ> typColumn;

    @FXML
    private TableColumn<Ent, String> gatunekColumn;

    @FXML
    private TableColumn<Ent, Integer> wysokoscColumn;

    @FXML
    private TableColumn<Ent, Integer> rokPosadzeniaColumn;


    public void setEnts(ObservableList<Ent> ents) {
        this.ents = ents;
        entsTable.setItems(this.ents);//Associated collection with table.
    }

    private ObservableList<Ent> ents = FXCollections.observableArrayList();

//    public void setEnt(ObservableList<Ent> ents) {
//        this.ents = ents;
//        entsTable.setItems(this.ents);//Associated collection with table.
//    }


    @FXML
    private ResourceBundle rb;

//    @FXML
//    private void open(ActionEvent event) {
//        ents.add(new Ent(42, 332, "sosna", Ent.Typ.LISCIASTE));
//        ents.add(new Ent(133, 120, "jesion", Ent.Typ.IGLASTE ));
//        ents.add(new Ent(1999, 420, "buk", Ent.Typ.LISCIASTE));
//    }

    @FXML
    private void open(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(((MenuItem) event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            open(file);
        }
    }


    private void open(File file) {
        OpenFile worker = new OpenFile(ents, file);
        progress.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }

    @FXML
    private void save(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(((MenuItem) event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {//Chooser returns null when user clicks cancel.
            save(file);//Keep methods simple.
        }
    }

    private void save(File file) {
        SaveFile worker = new SaveFile(ents, file);
        progress.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }


    @FXML
    private void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);//Create message box with information only.
        alert.setTitle(rb.getString("aboutTitle"));//Do not used hardcoded messages but get them from bundle.
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);//Workaround for wrapping text.
        alert.showAndWait();//Blocking dialog waits for user reaction.
    }

    @FXML
    private void plant(ActionEvent event) {
        ents.add(new Ent());
    }

    @FXML
    private void cut(ActionEvent event) {
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            ents.remove(entsTable.getSelectionModel().getSelectedIndex());
            entsTable.getSelectionModel().clearSelection();
        }
    }


    @FXML
    private void root(ActionEvent event) {
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            ents.add(new Ent(2018, 10, "dsad", Ent.Typ.LISCIASTE));


        }
    }

    @FXML
    private ProgressBar progress;


    public void initialize(URL url, ResourceBundle resourceBundle) {
        rb = resourceBundle;
        entsTable.setItems(ents);

        typColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.Typ.values()));
        typColumn.setOnEditCommit(event -> event.getRowValue().setTyp(event.getNewValue()));

        gatunekColumn.setCellFactory(TextFieldTableCell.forTableColumn()); //TextField - rodzaj pola przy edycji
        gatunekColumn.setOnEditCommit(event -> event.getRowValue().setGatunek(event.getNewValue()));

        wysokoscColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        wysokoscColumn.setOnEditCommit(event -> event.getRowValue().setWysokosc(event.getNewValue()));

        rokPosadzeniaColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        rokPosadzeniaColumn.setOnEditCommit(event -> event.getRowValue().setRokPosadzenia(event.getNewValue()));
    }
}


