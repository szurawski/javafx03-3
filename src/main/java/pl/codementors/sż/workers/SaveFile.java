package pl.codementors.sż.workers;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import pl.codementors.sż.Ent;


import java.io.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

public class SaveFile extends Task<Void>{

    private static final Logger log = Logger.getLogger(SaveFile.class.getCanonicalName());

    private Collection<Ent> ents;
    private File file;

    private DoubleProperty progress = new SimpleDoubleProperty(0);


//    public DoubleProperty progressProperty() {
//        return progress;
//    }


    public SaveFile(Collection<Ent> ents, File file) {
        this.ents = ents;
        this.file = file;
    }
    @Override
    protected Void call() throws Exception {
        int i = 0;
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {

            oos.writeObject(ents.size());
            for (Ent ent : ents) {
                try {
                    oos.writeObject(ent);
                    Thread.sleep(500);//Only to see that something is happening.
                    updateProgress(i+1,1);
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, ex.getMessage(), ex);
                }
            }
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }


}


